package com.flowlong.bpm.engine.core.mapper;

import com.flowlong.bpm.engine.entity.HisTaskActor;

public interface HisTaskActorMapper extends CommonMapper<HisTaskActor> {

}
